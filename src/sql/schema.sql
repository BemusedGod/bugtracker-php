CREATE TABLE IF NOT EXISTS user (
	user_id INT NOT NULL AUTO_INCREMENT,
	user_name VARCHAR(250) NOT NULL,
	user_password VARCHAR(250) NOT NULL,
	PRIMARY KEY(user_id),
	UNIQUE (user_id)
);

 CREATE TABLE IF NOT EXISTS project (
	project_id INT NOT NULL AUTO_INCREMENT,
	project_name VARCHAR(250) NOT NULL,
	project_desc VARCHAR(1000) NOT NULL,
	user_id INT NOT NULL,
	CONSTRAINT fk_project_creator
	FOREIGN KEY (user_id)
		REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (project_id),
	UNIQUE (project_id)
);

 CREATE TABLE IF NOT EXISTS issue (
	issue_id INT NOT NULL AUTO_INCREMENT,
	issue_name VARCHAR(250) NOT NULL,
	issue_desc VARCHAR(1000) NOT NULL,
	user_id INT NOT NULL,
	project_id INT NOT NULL,
	CONSTRAINT fk_issue_creator
	FOREIGN KEY (user_id)
		REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT fk_issue_project
	FOREIGN KEY (project_id)
		REFERENCES project(project_id) ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (issue_id),
	UNIQUE (issue_id)
);

 CREATE TABLE IF NOT EXISTS comment (
	comment_id INT NOT NULL AUTO_INCREMENT,
	comment_name VARCHAR(250) NOT NULL,
	comment_desc VARCHAR(1000) NOT NULL,
	user_id INT NOT NULL,
	issue_id INT NOT NULL,
	CONSTRAINT fk_comment_creator
	FOREIGN KEY (user_id)
		REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT fk_comment_issue
	FOREIGN KEY (issue_id)
		REFERENCES issue(issue_id) ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (comment_id),
	UNIQUE (comment_id)
);
